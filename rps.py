# Write a Rock / Paper / Scissors program
import random

# Define functions
def calc_move():
	rand_number = random.randrange(3,300)
	num_move = rand_number % 3 + 1
	
	return num_move
		
def game():
	# Get the Globals
	global comp_score
	global user_score
	
	# Run the app.
	computer_num = calc_move()
	print "Welcome to rock, paper, scissors.  Please choose your move: "
	user_move = raw_input()
		
	# Determine the computer move to a position
	if (computer_num == 1):
		computer_move = "rock"
	elif (computer_num == 2):
		computer_move = "paper"
	elif (computer_num == 3):
		computer_move = "scissors"
	else:
		computer_move = "none"
	
	if (user_move == "rock" and (computer_move == "scissors")):
		# Rock vs. Scissors - User win
		print "You win."
		user_score = user_score + 1
		
	elif (user_move == "paper" and (computer_move == "rock")):
		# Paper vs. Rock - User win
		print "You win."
		user_score = user_score + 1
		
	elif (user_move == "scissors" and (computer_move == "paper")):
		# Scissors vs. Paper - User win
		print "You win."
		user_score = user_score + 1
		
	elif (user_move == computer_move):
		# Tie
		print "We tied."
		
	elif (user_move == ""):
		print "You didn't pick a move... c'mon now."
		
	else:
		# Computer win
		print "I win."
		comp_score = comp_score + 1
		
	# State what the moves chosen were.
	print "You chose %s.  I chose %s." % (user_move, computer_move)
	print ("The score is %s to %s") % (user_score, comp_score)
	
	print "Press any key to play again, type 'quit' to finish"
	play_again = raw_input()

	if (play_again != "q" or play_again != "quit"):
		game()
	else:
		print "The final score was %s to %s" % (user_score, comp_score)
	

comp_score = 0
user_score = 0


game()